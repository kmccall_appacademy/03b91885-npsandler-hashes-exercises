# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hash = Hash.new
  str.split(' ').each { |word| hash[word] = word.length }
  hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash = hash.sort_by { |k, v| v }
  hash[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k, v| older[k] = v }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = Hash.new(0)
  word.chars.each { |char| hash[char] += 1 }
  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new(0)
  arr.each { |el| hash[el] += 1 }
  hash.select { |k, v| v = 1 }
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = Hash.new(0)
  numbers.each do |num|
    if num % 2 == 0
      hash[:even] += 1
    else
      hash[:odd] += 1
    end
  end
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  counter = Hash.new(0)
  string.chars.each { |char| counter[char] += 1 if vowels.include?(char) }
  counter.select { |k,v| v == counter.values.max }.keys.sort[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  arr = students.select { |k, v| v > 6 }.keys
  result = []
  i = 0
  while i < arr.length
    j = i + 1
    while j < arr.length
      result << [arr[i],arr[j]]
      j += 1
    end
    i += 1
    j = i
  end
  result
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  spec = specimens.uniq.length
  populations = specimens.map do
     |animal| pop_count(specimens, animal)
   end
   populations.sort!
   return spec ** 2 * populations[0] / populations[-1]
end

def pop_count(arr, str)
  counter = 0
  arr.each { |spec| counter += 1 if str == spec }
  counter
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign = character_count(normal_sign)
  vandalized_sign = character_count(vandalized_sign)
  vandalized_sign.all? { |k, v| normal_sign[k] >= vandalized_sign[k] }
 end

 def character_count(str)
   counter = Hash.new(0)
   new_str = str.dup
   punctuation = "'!?., "
   punctuation.chars.each { |char| new_str.gsub!(char, '') }

   new_str.chars.each { |el| counter[el.downcase] += 1 }
   counter
 end
